# Capítulo 1

## Instalación de GO

Realizar la descarga de la versión requerida.

```bash
wget https://go.dev/dl/go1.20.6.linux-amd64.tar.gz
```

Remover la versión anterior si es que existe y descomprimir.

```bash
rm -rf /usr/local/go && tar -C /usr/local -xzf go1.20.6.linux-amd64.tar.gz
```

Configurar variables de entorno. Agregar en tu .bash_profile o .bashrc

```bash
export PATH=$PATH:/usr/local/go/bin
```

Validar la instalación.

```bash
go version
```

## Comandos Basicos

`go build` Al ejecutar en el directorio raiz del proyecto, generara un archivo ejecutable.
`go run archivo.go` Ejecutara un programa en go, directamente, sin necesidad de compilarlo.
`go fmt ./...` Ejecutandose en el directorio raiz del proyecto, reformateara todos los archivos de codigo fuente.
`go vet ./...` Ejecutandose en el directorio raiz del proyecto, busca patrones suceptibles a ocasionar errores o comportamientos incorrectos.
`go doc` Levanta un servicio web http://localhost:6060 que incluye la documentacion.
`go get` Permite descargar bibliotecas y utilidades de linea de comandos de terceros.
`go mod` Permite gestionar sus proyectos locales, asi como sus dependencias hacia bibliotecas de terceros.

## Comentarios

`// Comentario de linea`
```golang
/*
multi
linea
*/
```
